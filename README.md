# Soedinglab projects

We are a research group at the Max-Planck Institute for Multidisciplinary Sciences working using math and computer science to work on biological questions and tools.

## Current projects
- [Fast implementation of the gradients of the felsenstein algorithm with respect to the rate matrix](/current_projects/gradients_of_rate_felsenstein.md)

## Other projects
- If you are generally interested, but did not find a good fit, you can write Benjamin Lieser (benjamin.lieser@mpinat.mpg.de) with a summary of what you are interested and your skills in applied math and programming. We might find something which fits.
