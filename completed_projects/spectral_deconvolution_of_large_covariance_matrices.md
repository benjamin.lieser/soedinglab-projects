## Spectral deconvolution of large covariance matrices

The goal is to implement fast and robust procedures for python with no dependency on matlab nor R (so using python/c++) for the two following problems:
 * Estimation of the true covariance spectrum of a multivariate random variable from a sample covariance spectrum, by numerically inverting a discretized version of the asymptotic Marchenko-Pastur spectral dispersion equations
 * Direct estimation of a non-linearly shrunk covariance matrix without recovering the true covariance spectrum

Implementations in matlab and R already exist, but none of them is currently usable in large bioinformatics pipelines and they are more proofs of concept ; hence the need for a scalable and robust implementation.

References:
 * Silverstein, J. W. (1995). Strong convergence of the empirical distribution of eigenvalues of large dimensional random matrices. Journal of Multivariate Analysis, 55(2), 331-339.
 * Ledoit, O., & Wolf, M. (2015). Spectrum estimation: A unified framework for covariance matrix estimation and PCA in large dimensions. Journal of Multivariate Analysis, 139, 360-384.
 * Ledoit, O., & Wolf, M. (2017). Numerical implementation of the QuEST function. Computational Statistics & Data Analysis, 115, 199-223.
 * Ledoit, O., & Wolf, M. (2017). Direct nonlinear shrinkage estimation of large-dimensional covariance matrices (No. 264). Working Paper.

 If your are interested please email Étienne Morice (etienne.morice@mpinat.mpg.de)
