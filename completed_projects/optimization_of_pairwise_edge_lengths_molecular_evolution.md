## Optimization of pairwise edge lengths in a model of molecular evolution

We have developed a new model of molecular evolution which incorperates the effects of pairs of amino acids into the fitness of the protein. There already exists C++ Code to learn some parameters of the model from data using stochastic variational inference.

This project is about learning the distance between two sequences under this model and implementing it. Preferable C++, but a fast python implementation should be possible.

This project has already been claimed/assigned.
