# Fast gradient evaluation of the Felsenstein Algorithm with respect to the rate Matrix in Rust/C++

## Felsenstein Algorithm
This algorithm evaluates the likelihood of a Phylogenetic tree with respect to a model describing the probabilities of mutations along the tree. It is a recursive algorithm along a tree. You can find good explanations online.

## Matrix Exp
The mutation probabilities are given by matrix_exp(time * rate_matrix), this (and especially the gradient of it) is an expensive operation and needs to be optimized by an eigenvalue decomposition in a numerical stable way (The math is already worked out, but need to be implemented)

## Performance
There is already a pytorch and a jax automatic differentiation implementation. Unfortunately both frameworks are not ideal for this problem and we think a lot of performance can be gained. The goal is to implemented the gradients in Rust or C++ (or other hardware near language). The correctness can be easily tested against the python implementation.

## What you need to know
- There is no biological knowledge required. The algorithm is quick to understand.
- Some knowledge about automatic differentiation is helpful but can be learned.
- You should have a good idea about gradients of multidimensional functions. But this is also quick to learn.
- Most importantly you should have interest to learn and understand how a computer executes code and how to make it fast. Experience in either Rust/C++/C/Assembly is very helpful.

## What you get
- A lot of experience of how to execute calculations fast.
- The possibility to contribute to cutting edge research on RNA secondary structure prediction (If it works well, also authorship to the paper)
- A good technical background in the area of machine learning

## If you are interested
Please contact benjamin.lieser@mpinat.mpg.de
